#!/usr/bin/python3

import sys
import tunnel_manager


# check python 3 is enabled

if sys.version_info[0] < 3:
    print("Warning, this software requires the python 3 interpreter.")
    print("Please, visit 'https://www.python.org' and follow the instructions for the installation.")
    print("This process will require an internet connection.")

else:
    try:
        import scapy.all

        scapy.all.IPv6()

    except:

        # try to autoinstall missing packet scapy
        print("Trying to autoinstall missing dependency: scapy ... ")

        # try automatic import if possible
        try:
            import sys, subprocess

            subprocess.call([sys.executable, '-m', 'pip', 'install', 'scapy'])
            import scapy.all

            scapy.all.IPv6()

            print(" Success")

        except:
            print(" Failed")
            exit(1)

    tunnel_manager.create_tunnel()

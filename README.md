﻿

# SRv6 L2 encap #

This project creates a level 2 tunnel using ipv6 type packets as explained on the site : [SRv6 Advanced Configuration](http://www.segment-routing.org/index.php/Implementation/AdvancedConf) and [SRv6 Network Programming](https://tools.ietf.org/html/draft-filsfils-spring-srv6-network-programming-04#page-12)

Possibility to use segment routing with [Segment Routing Extension Header type 4](https://tools.ietf.org/html/draft-ietf-6man-segment-routing-header-10#page-9)

### Prerequisite ###

for automatic configuration use file requirements.sh

Or follow the following steps:

Minimum version of the linux kernel ≥ 4.14

Python version ≥ 3.6

Minimum version of the iproute2 package ≥ 4.15

    
    > wget https://git.kernel.org/pub/scm/linux/kernel/git/shemminger/iproute2.git/snapshot/iproute2-4.15.0.tar.gz
    > tar xvf iproute2-4.15.0.tar.gz
    > cd iproute2-4.15.0/
    > make
    > sudo make install

Version of mininet >= 2.2.2

    > sudo apt-get install mininet

This project depend on old version of [SRv6 Mininet extensions](https://bitbucket.org/pierventre/srv6-mininet-extensions) which depends on [Dreamer Topology Parser and Validator](https://github.com/netgroup/Dreamer-Topology-Parser) already included in this project,

Scapy: Packet crafting for Python ≥ 2.4
Networkx: for Python == 1.11

    > sudo pip3 install scapy
    > sudo pip install networkx==1.11
    
    
ssh correctly configured

    > sudo apt-get install -y openssh-server
    > sudo nano /etc/ssh/sshd_config
    # change PermitRootLogin prohibit-password to PermitRootLogin yes
    # change #UseDNS no -> UseDNS no
    > sudo service ssh restart
    > sudo ufw allow 22
    
configure host for segment routing ipv6 
     
    # open file
    > /etc/sysctl.conf
    # insert the following lines at the end:
    > net.ipv6.conf.*.seg6_enabled = 1
    > net.ipv6.conf.*.seg6_require_hmac = -1
    

### Run an example experiment ###

You can start a topology just providing a topology file (relative path):

    > sudo python srv6_mininet_extension.py 

a network will be created as shown in file: topology.pdf from project [sr-testbed](https://bitbucket.org/nip1617/sr-testbed),

You can login on any router using their management IP

    # Connect to the router wdc
    > ssh root@2000::3
    
now set L2 decapsulation

    > ip -6 route add 2001:0:0:1::3 encap seg6local action End.DX2 oif wdc-eth1 dev wdc-eth1

You can see all packets for host lan interface eth3 using the command:

    > sudo scapy
    > sniff(iface='lan-eth3', prn=lambda x: x.display())

Connect at the beginning of the tunnel

    # Connect to the router atl
    > ssh root@2000::a
    
Run L2 encap

**--help** for usage options:
    
    Usage: l2_encap.py [options]
    
    Options:
    -h, --help                                show this help message and exit
    --interface=INTERFACE                     name of the interface to build the tunnel
    --destination=DESTINATION                 ipv6 address of destination
    --filter-only-ethernet=USE_ETHER          create a tunnel only for ethernet frame
    --counter=COUNTER                         number of frames to send in the tunnel, 0 for infinite
    --segroute=SEGROUTE                       list of intermediate destinations (escluding the final destination)
    --use-test=USE_TEST                       use test function
    
Create tunnel from incoming interface: atl-eth1 to destination: 2001:0:0:1::3, it captures L2 packets from the interface and encapsulates them in the SRv6 tunnel.
The source IPv6 address is taken from the IPv6 address of the (outgoing?) interface, so an IPv6 address is needed (on the outgoing interface?? or on the incoming one??). In the future, this assumption can be
removed and the IPv6 source address specified as a command line parameter.
    
    > sudo python3 ./l2_encap.py --interface atl-eth1 --destination 2001:0:0:1::3 --filter-only-ethernet yes --count 1 --segroute 2001:0:0:1a::1,2001:0:0:17::1 
    
if you want create a sequence of test packets directly from switch you can add option --use-test yes

    > sudo python3 ./l2_encap.py --interface atl-eth1 --destination 2001:0:0:1::3 --use-test yes --filter-only-ethernet yes --count 1 --segroute 2001:0:0:1a::1,2001:0:0:17::1

or create packets from a host, connect to hatl

    > ssh root@2000::15

run test sequence for generate packets in local network

    > sudo python3 test_sequence.py --interface hatl-eth1

### Issues ###

Scapy wrongly interprets segment routing headers: last entry field is counting starting from 0 while scapy assumes it starts from 1



developed by Fabio Di Giacomo of University of Rome Tor Vergata

# VXLAN with SRv6 encap #
The goal is to implement the traffic engegneering at level L2.
How it works
Device_1 and Device_2 belong to the same network. Exploiting VXLAN we can implement this feature and for the traffic engegneering we exploit the IPv6 source routing over VXLAN.

For this work we had to install a patch of the kernel that implements the decapsulation of the packet in the end node.
The patch can be found here: http://canary.netgroup.uniroma2.it:8080/

## Example ##

Start the topology just providing the topology file (relative path):

    > sudo python srv6_mininet_extension.py 
    
![alt text](https://bitbucket.org/nip1617/1718-srv6-l2encap/raw/dabf9e4d8eb570538a52e70ba85942bda9ebf980/Topology-SR.jpeg)

In the following image with red it's specified the path that the packet will follow using the source routing and in blue it's the default path that uses the default rules of the routing protocol. 

### Configure VXLAN on the desired devices ###

HOU configuration
```
ip -6 link add vxlan100 type vxlan id 100 local 2001:0:0:1e::1 remote 2001:0:0:15::1 dstport 0
ip addr a 192.168.200.101/24 dev vxlan100
ip link set vxlan100 up
```

WDC configuration 
```
ip -6 link add vxlan100 type vxlan id 100 local 2001:0:0:15::1 remote 2001:0:0:1e::1 dstport 0
ip addr a 192.168.200.102/24 dev vxlan100
ip link set vxlan100 up
```

### Configure source routing ###

```
hou ip -6 route add 2001:0:0:15::1/128 encap seg6 mode encap segs 5555::22,5555::33,5555::44 dev hou-eth4
hou ip -6 route add 5555::22 via 2001:0:0:1c::1 

kan ip -6 route add 5555::22 encap seg6local action End dev kan-eth4
kan ip -6 route add 5555::33 via 2001:0:0:1d::2

ind ip -6 route add 5555::33 via 2001:0:0:14::1 

chi ip -6 route add 5555::33 encap seg6local action End dev chi-eth2
chi ip -6 route add 5555::44 via 2001:0:0:12::1

nyc ip -6 route add 5555::44 via 2001:0:0:13::2

wdc ip addr add 5555::44/128 dev wdc-eth3


```

To run all the commands, simply launch the script from within mininet:

    > source vxlan+sr_v1
    
If now you do a ping from HOU:192.168.200.101 towards WDC:192.168.200.102

    > ping 192.168.200.102

The following things will happen:

- ICMP echo REQUEST
    1. The packet it's encapsulated inside the VXLAN tunnel
    2. The packet it's encapsulated inside an IPv6 with SR list (SR encap mode, otherwise with the SR inline mode it will not work because, WDC can't just delete the SRH and forward the ramain IPv6 pkt)
    3. The packet is "source" routed to the destination(WDC)
    4. The packet is decapsulated by the default decap rule of teh kernel because the last segment is assigned to an interface, otherwise it will not work (see updated version).

- ICMP echo REPLY
    1. The packet it's routed using the default routing protocol configurated, so it crosses ATL and arrives to HOU

## Updated version ##

In the updated version, we no need to exploit the default decap rule of the kernel assigning the last segment to an interface of the end node.
In fact we had to install a patch of the kernel that implements the decapsulation of the packet in the end node.
The patch can be found here: http://canary.netgroup.uniroma2.it:8080/

### Updated configuration of source routing ###

```
hou ip -6 route add 2001:0:0:15::1/128 encap seg6 mode encap segs 5555::22,5555::33,5555::44 dev hou-eth4
hou ip -6 route add 5555::22 via 2001:0:0:1c::1 

kan ip -6 route add 5555::22 encap seg6local action End dev kan-eth4
kan ip -6 route add 5555::33 via 2001:0:0:1d::2

ind ip -6 route add 5555::33 via 2001:0:0:14::1 

chi ip -6 route add 5555::33 encap seg6local action End dev chi-eth2
chi ip -6 route add 5555::44 via 2001:0:0:12::1

nyc ip -6 route add 5555::44 via 2001:0:0:13::2

wdc ip -6 route add 5555::44/128 encap seg6local action End.DT6 table 255 dev wdc-eth3


```

To run all the commands, simply launch the script from within mininet:

    > source vxlan+sr_v2

developed by Ivan palamà and Gheorghe Sirbu of University of Rome Tor Vergata

## VXLAN with SRv6 insert mode #
In this case the SR information is added inserting the SRH right after the IPv6 header of the original packet.
For this work we had to install a patch of the kernel that implements the POP of the header in the end node.
The patch can be found here: http://canary.netgroup.uniroma2.it:8080/

Similarly in this case we have to configure VXLAN on the desired devices and then configure source routing:

```
#setting vxlan in hou
hou ip -6 link add vxlan100 type vxlan id 100 local 2001:0:0:1e::1 remote 2001:0:0:15::1 dstport 0
hou ip addr a 192.168.200.101/24 dev vxlan100
hou ip link set vxlan100 up

#setting vxlan in wdc
wdc ip -6 link add vxlan100 type vxlan id 100 local 2001:0:0:15::1 remote 2001:0:0:1e::1 dstport 0
wdc ip addr a 192.168.200.102/24 dev vxlan100
wdc ip link set vxlan100 up

#configure source routing
hou ip -6 route add 2001:0:0:15::1/128 encap seg6 mode inline segs 5555::22,5555::33,5555::44 dev hou-eth4
hou ip -6 route add 5555::22 via 2001:0:0:1c::1

kan ip -6 route add 5555::22 encap seg6local action End dev kan-eth4
kan ip -6 route add 5555::33 via 2001:0:0:1d::2

ind ip -6 route add 5555::33 via 2001:0:0:14::1 

chi ip -6 route add 5555::33 encap seg6local action End dev chi-eth2
chi ip -6 route add 5555::44 via 2001:0:0:12::1

nyc ip -6 route add 5555::44 via 2001:0:0:13::2

wdc ip -6 route add 5555::44/128 encap seg6local action End flavors PSP dev wdc-eth3

```
To run all the commands, simply launch the script from within mininet:

    > source vxlan+SR_insert_mode
    
If now we do a ping from HOU:192.168.200.101 towards WDC:192.168.200.102 the following things will happen:


- The SRH header is inserted in the original IPv6 packet, immediately after the
IPv6 header and before the transport level header.
- The original IPv6 header is modified, in particular the IPv6 destination address is replaced with the IPv6 address of the
first segment in the segment list, 
- The original IPv6 destination address is carried in the SRH header as the last segment of the segment list.


## Variants: SRv6 (insert mode) with IPSec #

Alternatively we can set un IPSec tunnel instead of use VXLAN. This can be achieved using the ip xfrm commands to manipulate incoming and outgoing packets. 

In particular policy is used to access the Security Policy Database (SPD) while state is used
to access the Security Association Database (SAD). An IPsec connection
must have both SAD and SPD entry in order to work.

```
hou ip xfrm policy add dir out src 2001:0:0:1e::1 dst 2001:0:0:15::1 tmpl proto esp mode transport
hou ip xfrm state add src 2001:0:0:1e::1 dst 2001:0:0:15::1 proto esp spi 1 enc 'cbc(aes)' 0x3ed0af408cf5dcbf5d5d9a5fa806b224 mode transport

wdc ip xfrm policy add dir in src 2001:0:0:1e::1 dst 2001:0:0:15::1 tmpl proto esp mode transport
wdc ip xfrm state add src 2001:0:0:1e::1 dst 2001:0:0:15::1 proto esp spi 1 enc 'cbc(aes)' 0x3ed0af408cf5dcbf5d5d9a5fa806b224 mode transport
```

Then same configuration for srv6 (insert mode) 
```
hou ip -6 route add 2001:0:0:15::1/128 encap seg6 mode inline segs 5555::22,5555::33,5555::44 dev hou-eth4
hou ip -6 route add 5555::22 via 2001:0:0:1c::1

kan ip -6 route add 5555::22 encap seg6local action End dev kan-eth4
kan ip -6 route add 5555::33 via 2001:0:0:1d::2

ind ip -6 route add 5555::33 via 2001:0:0:14::1 

chi ip -6 route add 5555::33 encap seg6local action End dev chi-eth2
chi ip -6 route add 5555::44 via 2001:0:0:12::1

nyc ip -6 route add 5555::44 via 2001:0:0:13::2

wdc ip -6 route add 5555::44/128 encap seg6local action End flavors PSP dev wdc-eth3
```

To run all the commands, simply launch the script from within mininet:

    > source SR+IPSec

At the same time we can have an IPsec communication inside the vxlan tunnel or above the vxlan and UDP header.
For the first case we have to set the IPSec tunnel (respectively SAD and SPD entry) with IPv4 addresses instead in the latter case with IPv6 addresses. 
Here the first scenario example configuration:

```
hou ip xfrm policy add dir out src 192.168.200.101 dst 192.168.200.102  tmpl proto esp mode transport
hou ip xfrm state add src 192.168.200.101 dst 192.168.200.102 proto esp spi 1 enc 'cbc(aes)' 0x3ed0af408cf5dcbf5d5d9a5fa806b224 mode transport

wdc ip xfrm policy add dir in src 192.168.200.101 dst 192.168.200.102 tmpl proto esp mode transport
wdc ip xfrm state add src 192.168.200.101 dst 192.168.200.102 proto esp spi 1 enc 'cbc(aes)' 0x3ed0af408cf5dcbf5d5d9a5fa806b224 mode transport
```
And here the latter case:

```
hou ip xfrm policy add dir out src 2001:0:0:1e::1 dst 2001:0:0:15::1 tmpl proto esp
hou ip xfrm state add src 2001:0:0:1e::1 dst 2001:0:0:15::1 proto esp spi 1 enc 'cbc(aes)' 0x3ed0af408cf5dcbf5d5d9a5fa806b224

wdc ip xfrm policy add dir in src 2001:0:0:1e::1 dst 2001:0:0:15::1 tmpl proto esp
wdc ip xfrm state add src 2001:0:0:1e::1 dst 2001:0:0:15::1 proto esp spi 1 enc 'cbc(aes)' 0x3ed0af408cf5dcbf5d5d9a5fa806b224
```

To run all the commands for the vxlan+SR+IPSec, simply launch the script from within mininet:

    > source vxlan+SR+IPSec

So if we now do a ping from HOU:192.168.200.101 towards WDC:192.168.200.102 we will have the desired encrypted communication. 
 The packet header will be something like:

![alt text](pkt_header.jpeg)


Updated version by Salvatore Nedia
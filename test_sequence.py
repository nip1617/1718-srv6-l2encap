#!/usr/bin/python3

from optparse import OptionParser

import helper

interface = 'wlp2s0'


def parseOptions():
    parser = OptionParser()

    parser.add_option('--interface', dest='interface', type='string', default="wlp2s0",
                      help='name of the interface to build the tunnel')

    # Parse input parameters
    (options, args) = parser.parse_args()

    return options


# command for send a sequence of packets
if __name__ == '__main__':

    print("start")

    opts = parseOptions()

    interface = opts.interface
    if interface == "":
        print("please insert an interface")
        exit(1)

    helper.test_function(interface)

#!/bin/bash

#####################################################################
# This script installs requirements of the application
#####################################################################

sudo apt -y --force-yes update 

sudo apt -y install python-pip

sudo apt -y install python3-pip

sudo apt -y install iproute2=4.15.0-2ubuntu1

sudo apt -y install mininet

sudo pip3 install scapy

sudo pip install networkx==1.11

sudo apt install -y openssh-server
sudo sed -i 's/PermitRootLogin prohibit-password/PermitRootLogin yes/g' /etc/ssh/sshd_config
sudo sed -i 's/#UseDNS no/UseDNS no/g' /etc/ssh/sshd_config
sudo service ssh restart
sudo ufw allow 22

echo "net.ipv6.conf.*.seg6_enabled = 1" | sudo tee -a /etc/sysctl.conf
echo "net.ipv6.conf.*.seg6_require_hmac = -1" | sudo tee -a /etc/sysctl.conf
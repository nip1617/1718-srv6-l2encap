import socket
import scapy.all as scapy
import time

number_pkt_test = 1


# function for check if is a valid ipv6 address
def is_valid_ipv6_address(address):
    try:
        socket.inet_pton(socket.AF_INET6, address)
    except socket.error:  # not a valid address
        return False
    return True


# function for recover ipv6 address of an interface
def get_interface_ipv6_address(interface):
    host_ipv6_addr = scapy.in6_getifaddr()

    for all_interf in host_ipv6_addr:
        if all_interf[2] == interface:
            return all_interf[0]

    return ""


# function for format correctly addresses of segments
def extract_addr(segroute):
    st = ''
    for s in segroute:
        
        st += s
    segroute = [st]
    segroute = segroute[0].split(',')
    for next in segroute:
        if not is_valid_ipv6_address(next):
            return [False, segroute]  # if there is an error in the input addresses
    return [True, segroute]  # list of correct addresses


# function for generate a sequence of packet for testing
def test_function(interface):
    for count in range(number_pkt_test):
        time.sleep(1)
        p1 = scapy.Ether(dst='80:00:20:7A:3F:3E',src="11:22:33:44:55:66")
        p2 = scapy.IPv6(dst='1:2::1')
        p = p1 / p2
        scapy.sendp(p, iface=interface,verbose=0)
        print('Generated packet ' + str(count))

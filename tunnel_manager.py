#!/usr/bin/python3

from optparse import OptionParser
import os
import scapy.all as scapy
import time

import helper

# destination address
destination = ""

# interface of tunnel
interface = "wlp2s0"

# address ipv6 of interface
interface_ipv6 = ""

# number of packet to send on interface
counter = 0

# list of segment for segment routing
segroute = []

# for use test function
use_test = False

# for filter only ethernet packet
use_eth = False

# for debug use
use_debug = False


# Function for elaborate single packet
def elaborate(rx_pkt):
    global segroute

    if use_debug:
        print("###################### Data received")
        rx_pkt.show()
        

    if segroute:
        
        # create routing header type 4
        segroute.append(destination)
        new_hr_pkt = scapy.IPv6(dst=segroute[0])
        segroute=segroute[::-1]

        # old general version
        #seg_hr = scapy.IPv6ExtHdrRouting(addresses=segroute, segleft=len(segroute), len=2 * len(segroute), type=4,reserved=(len(segroute) - 1) << 24)

        # new version type 4 for default
        seg_hr = scapy.IPv6ExtHdrSegmentRouting(addresses=segroute, segleft=len(segroute)-1, len=2 * len(segroute),lastentry=(len(segroute))-1)

        # add header extension ipv6 type routing header (43)
        new_hr_pkt.nh = 43
        new_hr_pkt = new_hr_pkt / seg_hr
    else:
        # create packet ipv6 without extension
        new_hr_pkt = scapy.IPv6(dst=destination)

    # encapsulates packet received in ipv6 packet
    new_pkt = new_hr_pkt / rx_pkt

    if use_debug:
        print("##################### Data to send")
        new_pkt.show()

    # send new packet
    scapy.send(new_pkt)


# Parse command line options
def parseOptions():
    parser = OptionParser()

    parser.add_option('--interface', dest='interface', type='string', default="wlp2s0",
                      help='name of the interface to build the tunnel')

    parser.add_option('--destination', dest='destination', type='string', default="2000::1",
                      help='ipv6 address of destination')

    parser.add_option('--filter-only-ethernet', dest='use_ether', type='choice', choices=['yes', 'no'], default="yes",
                      help='create a tunnel only for ethernet frame')

    parser.add_option('--counter', dest='counter', type="int", default=0,
                      help='number of frames to send in the tunnel, 0 for infinite')

    parser.add_option('--segroute', dest='segroute', default=[], help='list of intermediate destinations (escluding the final destination)')

    parser.add_option('--use-test', dest='use_test', type='choice', choices=['yes', 'no'], default="no",
                      help='use test function')

    # Parse input parameters
    (options, args) = parser.parse_args()

    return options


# check input parameters
def check_param():
    opts = parseOptions()

    global interface

    # check input interface
    interface = opts.interface
    if interface == "":
        print("please insert an interface")
        exit(1)

    global destination

    # check input destination
    destination = opts.destination
    if destination == "":
        print("please insert an destination")
        exit(1)

    global counter

    # check input counter parameter
    counter = opts.counter
    if opts.counter < 0:
        print("option --counter must be positive number")
        exit(1)

    global interface_ipv6

    # take ipv6 address of input interface
    interface_ipv6 = helper.get_interface_ipv6_address(interface)

    if interface_ipv6 == "":
        print("This interface not have ipv6 address")
        exit(1)

    global segroute

    # check list of addresses for segment routing
    if opts.segroute:
        res = helper.extract_addr(opts.segroute)
        if not res[0]:
            print("Error in next step list, use addr1,addr2,..")
            exit(1)
        segroute = res[1]

    global use_test

    # use test function for generate test packet
    if opts.use_test == 'yes':
        use_test = True

    global use_eth

    # filter only ethernet packet
    if opts.use_ether == 'yes':
        use_eth = True


# function for collect packets on interface and elaborate it
def create_tunnel():
    check_param()

    print("start")

    print("ipv6 address = " + interface_ipv6 + " of the interface = " + interface)

    # create new process for send test packets
    if use_test:
        pid = os.fork()
        if pid:
            # parent
            pass
        else:
            # child
            time.sleep(2)
            helper.test_function(interface)
            exit(0)

    # print(scapy.sniff(iface="wlp2s0",prn=lambda x: x.sniffed_on+": "+x.summary(),lfilter=lambda x:x.haslayer(scapy.Ether),))
    # print(scapy.sniff(count=1, iface=interface, prn=elaborate, lfilter=lambda x: x.haslayer(scapy.Ether)))
    # print(scapy.sniff(count=10,iface=interface, prn=elaborate))

    # collect all packets on interface and filter them
    if use_eth:
        print(scapy.sniff(count=counter, iface=interface, prn=elaborate, lfilter=lambda x: x.haslayer(scapy.Ether)))

    else:
        print(scapy.sniff(count=counter, iface=interface, prn=elaborate))


if __name__ == '__main__':
    create_tunnel()